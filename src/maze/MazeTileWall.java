package maze;

import maze.exception.TryingToExploreBadMazeLocationException;

public class MazeTileWall extends MazeTile {
    private final MazeTileType type;
    
    public MazeTileWall() {
        this.type = MazeTileType.WALL;
    }
    
    @Override
    public boolean canExplore() {
        return false;
    }
    
    @Override
    public boolean isExplored() {
        return false;
    }
    
    @Override
    public boolean shouldExplore() {
        return false;
    }
    
    @Override
    public void setExplored() throws TryingToExploreBadMazeLocationException {
        throw new TryingToExploreBadMazeLocationException();
    }
    
    @Override
    public void setBadPath() {
		
    }

	@Override
	public char getDrawState() {
		return 'W';
	}
}

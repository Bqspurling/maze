/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import maze.exception.InvalidMazeSizeException;
import maze.exception.InvalidMazeTileException;
import maze.exception.NoMazeExitException;
import maze.exception.TryingToExploreBadMazeLocationException;

/**
 *
 * @author spurl
 */
public class MazeRunner {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            View view = null;
            try {
                view = new View("C:\\Users\\spurl\\OneDrive\\Documents\\MazeTestFile.txt");
            } catch (IOException ex) {
                Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidMazeSizeException ex) {
                Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidMazeTileException ex) {
                Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoMazeExitException ex) {
                Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TryingToExploreBadMazeLocationException ex) {
                Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
            }
            view.setVisible(true);
        });   
    }
}

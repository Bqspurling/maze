package maze;

import maze.exception.InvalidMazeTileStateException;
import maze.exception.TryingToExploreBadMazeLocationException;

public abstract class MazeTile {
    public abstract boolean canExplore();
    public abstract boolean isExplored();
    public abstract boolean shouldExplore();
    public abstract void setExplored() throws TryingToExploreBadMazeLocationException;
    public abstract void setBadPath();
    public abstract char getDrawState() throws InvalidMazeTileStateException;
}

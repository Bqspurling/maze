package maze;

public enum MazeTileStatus {
    UNEXPLORED,
    BADPATH,
    GOODPATH,
}

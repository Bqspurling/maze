package maze;

import maze.exception.InvalidMazeTileStateException;

public class MazeTileSpace extends MazeTile {

	private final MazeTileType type;
	private MazeTileStatus status;

	public MazeTileSpace() {
		this.type = MazeTileType.SPACE;
		this.status = MazeTileStatus.UNEXPLORED;
	}

	@Override
	public boolean canExplore() {
		return true;
	}

	@Override
	public boolean isExplored() {
		return this.status != MazeTileStatus.UNEXPLORED;
	}

	@Override
	public boolean shouldExplore() {
		return !this.isExplored();
	}

	@Override
	public void setExplored() {
		// Consider: Throw exception in trying to re-explore already-explored
		// tile because that means we've made a mistake; no tile should ever get
		// explored more than once
		// Until we back out of a space, assume it's going to be in our final
		// path; saves effort backtracking to the start.
		this.status = MazeTileStatus.GOODPATH;
	}

	@Override
	public void setBadPath() {
		this.status = MazeTileStatus.BADPATH;
	}

	@Override
	public char getDrawState() throws InvalidMazeTileStateException {
		if (this.status == MazeTileStatus.UNEXPLORED) {
			return 'U';
		}
		if (this.status == MazeTileStatus.GOODPATH) {
			return 'G';
		}
		if (this.status == MazeTileStatus.BADPATH) {
			return 'B';
		}
		throw new InvalidMazeTileStateException();
	}
}

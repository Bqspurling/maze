package maze;

public enum MazeTileType {
    UNASSIGNED,
    WALL,
    SPACE,
}
